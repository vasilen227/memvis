# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Aug 12 2016)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.grid

###########################################################################
## Class FrameMain
###########################################################################

class FrameMain ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Stack Visualisation", pos = wx.DefaultPosition, size = wx.Size( 442,405 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.PanelMain = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		SizerMain = wx.BoxSizer( wx.VERTICAL )
	
		bSizerMainPanel = wx.BoxSizer( wx.VERTICAL )
	
		bSizerDir = wx.BoxSizer( wx.VERTICAL )
		
		bSizerDirHor = wx.BoxSizer( wx.HORIZONTAL )
		
		sbSizerDir = wx.StaticBoxSizer( wx.StaticBox( self.PanelMain, wx.ID_ANY, u"Process ID" ), wx.HORIZONTAL )
	
		self.m_textCtrlDir = wx.TextCtrl( sbSizerDir.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizerDir.Add( self.m_textCtrlDir, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_button10 = wx.Button( sbSizerDir.GetStaticBox(), wx.ID_ANY, u"Proceed", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizerDir.Add( self.m_button10, 0, wx.ALL|wx.EXPAND, 5 )
			
		
		bSizerDirHor.Add( sbSizerDir, 1, wx.ALL|wx.EXPAND, 10 )
	
		
		bSizerDir.Add( bSizerDirHor, 0, wx.ALL|wx.BOTTOM|wx.EXPAND, 0 )
		
		StackSizer = wx.StaticBoxSizer( wx.StaticBox( self.PanelMain, wx.ID_ANY, u"Stack Visualisation" ), wx.HORIZONTAL )
	
		self.ScrollText = wx.ScrolledWindow( StackSizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.VSCROLL )
		self.ScrollText.SetScrollRate( 5, 8 )
		SizerScrollStack = wx.BoxSizer( wx.VERTICAL )
	
		bSizer10 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_grid1 = wx.grid.Grid( self.ScrollText, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )


		
		# Grid
		self.m_grid1.CreateGrid( 11, 16 )
		self.m_grid1.EnableEditing( False )
		self.m_grid1.EnableGridLines( True )
		self.m_grid1.EnableDragGridSize( False )
		self.m_grid1.SetMargins( 0, 0 )
			
		# Columns
		self.m_grid1.EnableDragColMove( False )
		self.m_grid1.EnableDragColSize( True )
		self.m_grid1.SetColLabelSize( 30 )
		self.m_grid1.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		self.m_grid1.SetColLabelValue(0, "00")
		self.m_grid1.SetColLabelValue(1, "01")
		self.m_grid1.SetColLabelValue(2, "02")
		self.m_grid1.SetColLabelValue(3, "03")
		self.m_grid1.SetColLabelValue(4, "04")
		self.m_grid1.SetColLabelValue(5, "05")
		self.m_grid1.SetColLabelValue(6, "06")
		self.m_grid1.SetColLabelValue(7, "07")
		self.m_grid1.SetColLabelValue(8, "08")
		self.m_grid1.SetColLabelValue(9, "09")
		self.m_grid1.SetColLabelValue(10, "0A")
		self.m_grid1.SetColLabelValue(11, "0B")
		self.m_grid1.SetColLabelValue(12, "0C")
		self.m_grid1.SetColLabelValue(13, "0D")
		self.m_grid1.SetColLabelValue(14, "0E")
		self.m_grid1.SetColLabelValue(15, "0F")

		
		# Rows
		self.m_grid1.EnableDragRowSize( True )
		self.m_grid1.SetRowLabelSize( 130 )
		self.m_grid1.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		
		
		# Label Appearance
		
		# Cell Defaults
		self.m_grid1.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
		bSizer10.Add( self.m_grid1, 0, wx.ALL, 5 )
		
		
		SizerScrollStack.Add( bSizer10, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.ScrollText.SetSizer( SizerScrollStack )
		self.ScrollText.Layout()
		SizerScrollStack.Fit( self.ScrollText )
		StackSizer.Add( self.ScrollText, 1, wx.ALL|wx.EXPAND, 0 )
		
		
		bSizerDir.Add( StackSizer, 1, wx.ALL|wx.EXPAND, 10 )
		
		
		bSizerMainPanel.Add( bSizerDir, 1, wx.ALL|wx.BOTTOM|wx.EXPAND, 0 )
		
		
		SizerMain.Add( bSizerMainPanel, 1, wx.ALL|wx.BOTTOM|wx.EXPAND, 0 )
		
		
		self.PanelMain.SetSizer( SizerMain )
		self.PanelMain.Layout()
		SizerMain.Fit( self.PanelMain )
		bSizer1.Add( self.PanelMain, 1, wx.EXPAND, 0 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		self.m_menubar2 = wx.MenuBar( 0 )
		self.m_menu4 = wx.Menu()
		self.m_menuItem1 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Quit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu4.AppendItem( self.m_menuItem1 )
		
		self.m_menubar2.Append( self.m_menu4, u"Application" ) 
		
		self.SetMenuBar( self.m_menubar2 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_button10.Bind( wx.EVT_BUTTON, self.m_button10OnButtonClick )
		#self.m_menuItem1.Bind( wx.EVT_BUTTON, self.m_menuItem1OnButtonClick)
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def m_button10OnButtonClick( self, event ):
		event.Skip()
	


	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def m_menuItem1(self, event):
		dlg = wx.MessageDialog(self, 
			"Do you really want to close this application?",
			"Confirm Exit", wx.OK|wx.CANCEL|wx.ICON_QUESTION)
		result = dlg.ShowModal()
		dlg.Destroy()
		if result == wx.ID_OK:
			self.Destroy()

app = wx.App()
main_frame = FrameMain(None)
main_frame.Show(True)
app.MainLoop()