import urwid, os
choices = u'graphical console Exit'.split()


def menu(title, choices):
    body = [urwid.Text(title), urwid.Divider()]
    for c in choices:
        button = urwid.Button(c)
        urwid.connect_signal(button, 'click', item_chosen, c)
        body.append(urwid.AttrMap(button, None, focus_map='reversed'))
    return urwid.ListBox(urwid.SimpleFocusListWalker(body))

def item_chosen(button, choice):
    if choice == "Exit":
    	response = urwid.Text([u'You have chosen to ', choice, ' the program!', u'\n'])
    	done = urwid.Button(u'Ok')
    	urwid.connect_signal(done, 'click', exit_program)
    	main.original_widget = urwid.Filler(urwid.Pile([response,
        	urwid.AttrMap(done, None, focus_map='reversed')]))
    elif choice == 'graphical':
        response = urwid.Text([u'You have chosen ', choice, ' user interface!', u'\n'])
    	done = urwid.Button(u'Ok')
    	urwid.connect_signal(done, 'click', open_GUI)
    	main.original_widget = urwid.Filler(urwid.Pile([response,
        	urwid.AttrMap(done, None, focus_map='reversed')]))

def open_GUI(button):	
	os.system("sudo python main.py")
	raise urwid.ExitMainLoop()

def exit_program(button):
    raise urwid.ExitMainLoop()



main = urwid.Padding(menu(u'Choose type of interface', choices), left=2, right=2)
top = urwid.Overlay(main, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
    align='center', width=('relative', 60),
    valign='middle', height=('relative', 60),
    min_width=20, min_height=9)
urwid.MainLoop(top, palette=[('reversed', 'standout', '')]).run()