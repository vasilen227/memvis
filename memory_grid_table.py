#!/usr/bin/python

from wx import *
from wx.grid import *
import backend
import memory_scanner as ms
import binascii

app = None

EVT_BACKEND_UPDATE = wx.NewEventType()

class MemoryGridTable(PyGridTableBase):
    def __init__(self, app, grid, pid):
        PyGridTableBase.__init__(self)
        self.pid = pid
        self.app = app
        self.backend = backend.Backend(self.pid)
        self.backend.addObserver(self.updateNotification)
        self.grid = grid
        self.EVT_BACKEND_UPDATE_BINDER = wx.PyEventBinder(EVT_BACKEND_UPDATE, 1)
        self.app.Bind(self.EVT_BACKEND_UPDATE_BINDER, self.refreshData)

    def start(self):
        self.backend.start()
        self.start_addr = 16 * (self.backend.fetchStackTop() // 16)
        end_addr = self.start_addr + 16*10 # ?!?!?!?
        print self.start_addr, end_addr
        self.memoryRange = self.backend.fetchMemoryAt(self.start_addr, end_addr)

    def stop(self):
        if self.backend.stopped == False:
            self.backend.stop_join()
        
    def updateNotification(self, *args):
        app = self.app
        evt = wx.PyCommandEvent(EVT_BACKEND_UPDATE, -1)
        wx.PostEvent(app, evt)
    
    def refreshData(self, *args):
        self.grid.ForceRefresh()
    
    def moveDown(self, n=1):
        cur_start = self.memoryRange.start
        cur_end = self.memoryRange.end
        self.memoryRange = self.backend.fetchMemoryAt(cur_start + n * 16, cur_end + n * 16)
        self.refreshData()
      
    def findAddress(self, chosenAddress):
    	cur_start = 16 * (chosenAddress // 16)
        cur_end = cur_start + 16 * 10
        self.memoryRange = self.backend.fetchMemoryAt(cur_start, cur_end)
        self.refreshData() 

    def moveUp(self, n=1):
        cur_start = self.memoryRange.start
        cur_end = self.memoryRange.end
        self.memoryRange = self.backend.fetchMemoryAt(cur_start - n * 16, cur_end - n * 16)
        self.refreshData()
    
    def GetNumberRows(self):
        return 10 # FIX THIS
	
    def GetNumberCols(self):
        return 16

    def IsEmptyCell(self, row, col):
        #try:
        #    return not self.data[row][col]
        #except IndexError:
        #    return True
        return False
    
    def GetValue(self, row, col):
        val = self.memoryRange.data[(row*16+col)]
        if val is None:
            return 'Loading...'
        try:
            if ord(val) < 32 or ord(val) == 0x7f:
                raise ValueError
            unichr(ord(val))
            return ('{} \n {}'.format(hex(ord(val)), unichr(ord(val))))
        except:
            return hex(ord(val))

    def SetValue(self, row, col, value):
        #raise NotImplementedError()
        try:
            realByteHex = int(value.split("\n")[0], 16)
        except:
            raise ValueError("Invalid value for cell")
        print realByteHex
        realAddress = self.start_addr + row*16 + col
        self.backend.setMemoryByte(realAddress, chr(realByteHex))
        self.backend.refreshData()
        #try:
        #    self.data[row][col] = value
        #except IndexError:
        #    self.data.append([''] * self.GetNumberCols())
        #    self.SetValue(row, col, value)
        #    msg = wxGridTableMessage(self,                             # The table
        #                             wxGRIDTABLE_NOTIFY_ROWS_APPENDED, # what we did to it
        #                             1)                                # how many
        #    self.GetView().ProcessTableMessage(msg)
    
    def GetColLabelValue(self, col):
        return hex(col)
    
    def GetRowLabelValue(self, row):
        #val = self.backend._registerMemoryRange(self.memoryRange)
        #return 'Row%d' % (row,)
        rowValue = self.memoryRange.start + row*16
        return format(rowValue, 'x')

    def __del__(self):
        print "Stopping backend"
        self.backend.stop_join()