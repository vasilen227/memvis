#include <stdio.h>
#include <string.h>

int main(void)
{
    char buff[15];
    int pass = 0;
    char check[32];

    printf("\n Enter the password : \n");
    gets(buff);

    if(strcmp(buff, "Intel_ISEF"))
    {
        sprintf(check, "You do not have root privileges!");
        printf ("\n Wrong Password \n");
    }
    else
    {
        printf ("\n Correct Password \n");
        pass = 1;
    }

    if(pass)
    {   
        sprintf(check, "You now have root privileges!");
        printf ("\n Root privileges given to the user \n");
    }
    
    while(1) {
        sleep(20);
    }

    return 0;
}