#include <stdio.h>

int f(int n){
	return 2 * n;
}

int main(){
	printf("%d\n", f(5));
	return 0;
}