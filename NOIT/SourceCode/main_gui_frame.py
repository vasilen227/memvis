# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Aug 12 2016)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.grid

from memory_grid_table import MemoryGridTable
import backend

###########################################################################
## Class FrameMain
###########################################################################

class FrameMain ( wx.Frame ):
    
    def __init__( self, parent , pid, app):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"MemVis", pos = wx.DefaultPosition, size = wx.Size( 442,405 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
        
        self.pid = pid
        self.app = app
        
        self.backend = None
        if self.pid:
            self.backend = backend.Backend(self.pid)
            self.backend.start()
        
        self.table = None
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        
        bSizer1 = wx.BoxSizer( wx.VERTICAL )
        
        self.PanelMain = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        SizerMain = wx.BoxSizer( wx.VERTICAL )
    
        bSizerMainPanel = wx.BoxSizer( wx.VERTICAL )
    
        bSizerDir = wx.BoxSizer( wx.VERTICAL )
        
        bSizerDirHor = wx.BoxSizer( wx.HORIZONTAL )
        
        sbSizerDir = wx.StaticBoxSizer( wx.StaticBox( self.PanelMain, wx.ID_ANY, u"Process ID" ), wx.HORIZONTAL )
    
        self.m_textCtrlDir = wx.TextCtrl( sbSizerDir.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        sbSizerDir.Add( self.m_textCtrlDir, 1, wx.ALL|wx.EXPAND, 5 )
        
        self.m_button10 = wx.Button( sbSizerDir.GetStaticBox(), wx.ID_ANY, u"Proceed", wx.DefaultPosition, wx.DefaultSize, 0 )
        sbSizerDir.Add( self.m_button10, 0, wx.ALL|wx.EXPAND, 5 )
        
        bSizerDirHor.Add( sbSizerDir, 1, wx.ALL|wx.EXPAND, 10 )
    
        
        bSizerDir.Add( bSizerDirHor, 0, wx.ALL|wx.BOTTOM|wx.EXPAND, 0 )
        
        StackSizer = wx.StaticBoxSizer( wx.StaticBox( self.PanelMain, wx.ID_ANY, u"МемVis" ), wx.HORIZONTAL )  
    
        self.ScrollText = wx.ScrolledWindow( StackSizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL)
        self.ScrollText.SetScrollRate( 5, 8 )
        SizerScrollStack = wx.BoxSizer( wx.VERTICAL )
    
        self.bSizer10 = wx.BoxSizer( wx.VERTICAL )

        
        self.scrollButtonsBox = wx.BoxSizer(wx.HORIZONTAL)
        
        self.buttonUp = wx.Button( self.PanelMain, wx.ID_ANY, u"Up", size=(50,25), pos=(150,70))
        self.scrollButtonsBox.Add(self.buttonUp, 0, wx.ALL|wx.LEFT, 10)

        self.buttonDown = wx.Button( self.PanelMain, wx.ID_ANY, u"Down", size=(50,25), pos=(205,70))
        self.scrollButtonsBox.Add(self.buttonDown, 0, wx.ALL|wx.RIGHT, 10)
        
        self.m_grid1 = None
        
        self.reinitGrid()
        
        SizerScrollStack.Add( self.bSizer10, 1, wx.ALL|wx.EXPAND, 5 )
        
        
        self.ScrollText.SetSizer( SizerScrollStack )
        self.ScrollText.Layout()
        SizerScrollStack.Fit( self.ScrollText )
        StackSizer.Add( self.ScrollText, 1, wx.ALL|wx.EXPAND, 0 )
        
        
        bSizerDir.Add( StackSizer, 1, wx.ALL|wx.EXPAND, 10 )
        
        
        bSizerMainPanel.Add( bSizerDir, 1, wx.ALL|wx.BOTTOM|wx.EXPAND, 0 )
        
        
        SizerMain.Add( bSizerMainPanel, 1, wx.ALL|wx.BOTTOM|wx.EXPAND, 0 )
        
        
        self.PanelMain.SetSizer( SizerMain )
        self.PanelMain.Layout()
        SizerMain.Fit( self.PanelMain )
        bSizer1.Add( self.PanelMain, 1, wx.EXPAND, 0 )
        
        
        self.SetSizer( bSizer1 )
        self.Layout()
        self.m_menubar2 = wx.MenuBar( 0 )
        self.m_menu4 = wx.Menu()
        self.m_menuItem1 = wx.MenuItem( self.m_menu4, wx.ID_ANY, u"Quit", wx.EmptyString, wx.ITEM_NORMAL )
        self.m_menu4.AppendItem( self.m_menuItem1 )
        
        self.m_menubar2.Append( self.m_menu4, u"Application" ) 
        
        self.SetMenuBar( self.m_menubar2 )
        
        
        self.Centre( wx.BOTH )
        
        # Connect Events
        self.m_button10.Bind( wx.EVT_BUTTON, self.m_button10OnButtonClick )
        self.buttonUp.Bind( wx.EVT_BUTTON, self.buttonUpOnButtonClick )
        self.buttonDown.Bind( wx.EVT_BUTTON, self.buttonDownOnButtonClick )


    
    def __del__( self ):
        pass

    def reinitGrid(self):
        if self.m_grid1:
            #self.bSizer10.Hide(self.m_grid1)
            self.bSizer10.Remove(self.m_grid1)
        
        if self.table:
            self.table = None
        
        self.m_grid1 = wx.grid.Grid( self.ScrollText, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        #memoryGridTable = MemoryGridTable()
        #self.m_grid1.SetTable(memoryTable)
        if not self.pid:
            print "Creating fake table"
            self.m_grid1.CreateGrid(10, 16)
            self.m_grid1.EnableEditing( False )
            self.m_grid1.EnableGridLines( True )
            self.m_grid1.EnableDragGridSize( False )
            self.m_grid1.SetMargins( 0, 0 )

            
            """# Grid
            self.m_grid1.CreateGrid( 10, 16 )
            self.m_grid1.EnableEditing( False )
            self.m_grid1.EnableGridLines( True )
            self.m_grid1.EnableDragGridSize( False )
            self.m_grid1.SetMargins( 0, 0 )
                """
            #Columns
            #self.m_grid1.EnableDragColMove( False )
            #self.m_grid1.EnableDragColSize( True )
            #self.m_grid1.SetColLabelSize( 30 )
            #self.m_grid1.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
            self.m_grid1.SetColLabelValue(0, "0x0")
            self.m_grid1.SetColLabelValue(1, "0x1")
            self.m_grid1.SetColLabelValue(2, "0x2")
            self.m_grid1.SetColLabelValue(3, "0x3")
            self.m_grid1.SetColLabelValue(4, "0x4")
            self.m_grid1.SetColLabelValue(5, "0x5")
            self.m_grid1.SetColLabelValue(6, "0x6")
            self.m_grid1.SetColLabelValue(7, "0x7")
            self.m_grid1.SetColLabelValue(8, "0x8")
            self.m_grid1.SetColLabelValue(9, "0x9")
            self.m_grid1.SetColLabelValue(10, "0xa")
            self.m_grid1.SetColLabelValue(11, "0xb")
            self.m_grid1.SetColLabelValue(12, "0xc")
            self.m_grid1.SetColLabelValue(13, "0xd")
            self.m_grid1.SetColLabelValue(14, "0xe")
            self.m_grid1.SetColLabelValue(15, "0xf")
            

            # Rows
            self.cellHeight = 40
            self.cellWidth = 40
            #self.m_grid1.EnableDragRowSize( True )
            #self.m_grid1.SetRowLabelSize( 130 )
            self.m_grid1.SetDefaultRowSize(self.cellHeight, resizeExistingRows=False)
            self.m_grid1.SetDefaultColSize(self.cellWidth)
            #self.m_grid1.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
            for i in range (0, 10):
                self.m_grid1.SetRowLabelValue(i, 'Row%d' % (i,))
        
        else:
            print "Creating new realtable"
            self.cellHeight = 40
            self.cellWidth = 40
            self.table = MemoryGridTable(self.backend, self.app, self.m_grid1)
            self.m_grid1.SetTable(self.table)
            self.m_grid1.SetDefaultRowSize(self.cellHeight)
            self.m_grid1.SetDefaultColSize(self.cellWidth)
            self.m_grid1.SetDefaultCellAlignment(wx.ALIGN_CENTRE, wx.ALIGN_TOP) 
        # Label Appearance
        
        # Cell Defaults
        self.m_grid1.SetDefaultCellAlignment(wx.ALIGN_CENTRE, wx.ALIGN_TOP)
        self.bSizer10.Add( self.m_grid1, 0, wx.ALL, 5 )
    
    # Virtual event handlers, overide them in your derived class
    def m_button10OnButtonClick( self, event ):
        if self.backend:
            self.backend.stop_join() #TODO: Fix this
        self.pid = int(self.m_textCtrlDir.GetValue())
        self.backend = backend.Backend(self.pid)
        self.backend.start()
        self.reinitGrid()
        event.Skip()


    def __del__( self ):
        if self.backend:
            self.backend.stop()
    
    def buttonUpOnButtonClick( self, event ):
        self.table.moveUp()
        event.Skip()

    def buttonDownOnButtonClick( self, event ):
        self.table.moveDown()
        event.Skip()
    
    # Virtual event handlers, overide them in your derived class
    def m_menuItem1(self, event):
        dlg = wx.MessageDialog(self, 
            "Do you really want to close this application?",
            "Confirm Exit", wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK:
            self.Destroy()