#!/usr/bin/python
import ptrace.debugger, os, binascii
from struct import unpack
import pdb

PAGESIZE = None
try:
    import subprocess
    PAGESIZE = int(subprocess.check_output("getconf PAGE_SIZE", shell=True))
except:
    print "Warning: Could not dynamically get Linux PAGE_SIZE, defaulting to 4096"
    PAGESIZE = 4096

#TODO: Make this much more agile
import struct
PTR_SIZE = struct.calcsize("P") # Size of a pointer
#NB: We are assuming an 8-bit byte
MAX_ADDRESS = 2 ** (8 * PTR_SIZE)
PAGE_COUNT = MAX_ADDRESS / PAGESIZE
#print MAX_ADDRESS, PAGESIZE, PAGE_COUNT, PTR_SIZE
assert MAX_ADDRESS % PAGESIZE == 0

def page_start(i):
    return i * PAGE_SIZE

def page_end1(i):
    return (i+1) * PAGE_SIZE

class Map:
    def __init__(self, map_line):
        # TODO: Fix this HORRIBLE, HORRIBLE code
        self.line = map_line
        self.addresses = self.line.split(" ", 1)[0]
        self.startAddress = self.addresses.split("-", 1)[0]
        self.endAddress = self.addresses.split("-", 1)[1]
        self.permissions = self.line.split(" ", 1)[1].split(" ", 1)[0]
        self.offset = self.line.split(" ", 1)[1].split(" ", 1)[1].split(" ", 1)[0]
        self.device = self.line.split(" ", 1)[1].split(" ", 1)[1].split(" ", 1)[1].split(" ", 1)[0]
        self.inode = self.line.split(" ", 1)[1].split(" ", 1)[1].split(" ", 1)[1].split(" ", 1)[1].split(" ", 1)[0]
        try:
            self.pathName = self.line.split(" ", 1)[1].split(" ", 1)[1].split(" ", 1)[1].split(" ", 1)[1].split()[1]
        except IndexError:
            self.pathName = "No given path name!"

class MemoryScanner:
    def __init__(self, pid):
        self.pid = pid
        self.debugger = ptrace.debugger.PtraceDebugger()
        self.targetProcess = None

    def attach(self):
        self.targetProcess = self.debugger.addProcess(self.pid, False)

    def detach(self):
        self.targetProcess.detach()
        self.debugger.quit()

    def getMemoryRange(self, startAddress, endAddress):
        if not self.targetProcess:
            raise ValueError("MemoryScanner is not attached!")
        current = ""
        memoryDevice = os.open("/proc/" + str(self.pid) + "/mem", 0)
        if endAddress > startAddress:
            os.lseek(memoryDevice, startAddress, os.SEEK_SET)
            current = os.read(memoryDevice, endAddress-startAddress)
            #currentValue.append(hex(unpack('P', current)[0]).rstrip("L"))
        os.close(memoryDevice)
        return current
        #struct.calcsize(startAddress)
        #return currentValue
        #rewrite with "with open ....."
    
    def getMaps(self):
        if not self.targetProcess:
            raise ValueError("MemoryScanner is not attached!")
        maps = []
        mapFile = open("/proc/" + str(self.pid) + "/maps").readlines()
        for line in mapFile:
            maps.append(Map(line).addresses)
        return maps
    
    def getRegisters(self):
        allRegisters = self.targetProcess.getregs()
        return allRegisters



def main():
    pid = input('Enter process ID: ')
    memscan = MemoryScanner(pid)
    memscan.attach()
    regs = memscan.getRegisters()
    print regs
    print regs.rsp
    rsp = regs.rsp
    #pdb.set_trace()
    maps = memscan.getMaps()
    print maps
    print binascii.hexlify(memscan.getMemoryRange(rsp, rsp+8))  
    memscan.detach()

if __name__ == "__main__":
    main()
