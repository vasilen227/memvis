#!/usr/bin/python

from threading import Thread
import time
import memory_scanner as ms
import weakref

class MemoryRange:
    def __init__(self, memoryRange, backend):
        self._memoryRange = memoryRange
        self._registers = None
        self._maps = None
        self._backend = backend
        self._data = [None] * (memoryRange[1] - memoryRange[0])
    
    @property
    def maps(self):
        return self._maps
    
    @property
    def registers(self):
        return self._registers
    
    @property
    def data(self):
        return self._data
    
    @property
    def start(self):
        return self._memoryRange[0]
    
    @property
    def end(self):
        return self._memoryRange[1]

class Backend:

    def __init__(self, pid=None, refreshInterval=2.0):
        self.pid = pid
        self.refreshInterval = refreshInterval
        self.memoryScanner = None
        self._visibleMemoryRanges = []
        self._runningThread = None
        self._pageCache = {}
        self._mapsCache = None
        self._regsCache = None
        self._observers = []
        self.stopped = True

    def start(self):
        if self.pid is None:
		    raise ValueError("Process ID must be set before attaching")
        assert self.stopped
        self.stopped = False
        self._runningThread = Thread(target=self._threadMain, args=(self,))
        self._runningThread.start()
    
    def stop(self):
        self.stopped = True

    def stop_join(self):
        self.stopped = True
        self._runningThread.join()

    def triggerUpdate(self): # Not implemented yet
        pass

    #TODO: Make the following work (caching and so on)
    """def _threadMain(self):
        PS = ms.PAGESIZE
        floorPS = lambda x: x // PS
        ceilPS = lambda x: -floorPS(-x)
        self._actualMemoryRange = (max(floorPS(memoryRange[0]) - 1, 0), min(ceilPS(memoryRange[1]) + 1, )) # Capture previous and next memory pages too
        while not self.stopped:
            new_page_cache = {}
            for range in self.visibleMemoryRanges:
                page_range = (max(floorPS(range[0])-1, 0), min(ceilPS(range[1])+1,ms.PAGE_COUNT))
                for page in range(page_range[0], page_range[1]):
                    new_page_cache[page] = 1 # Mark for caching
            for page in sorted(new_page_cache.keys()):
                new_page_cache = self.memoryScanner.getMemoryRange(ms.page_start(page), ms.page_end(page))
            self._mapsCache = self.memoryScanner.getMaps()
            self._regsCache = self.memoryScanner.getRegisters()
            self._pageCache = new_page_cache
            for page in 
            sleep(self.refreshInterval)"""
    def _threadMain(self, *args):
        print "Starting thread main"
        self.memoryScanner = ms.MemoryScanner(self.pid)
        self.memoryScanner.attach()
        while not self.stopped:
            self._mapsCache = self.memoryScanner.getMaps()
            self._regsCache = self.memoryScanner.getRegisters()
            for rangeref in self._visibleMemoryRanges:
                range = rangeref()
                if range:
                    range._data = self.memoryScanner.getMemoryRange(range.start, range.end)
                    range._registers = self._regsCache
                    range._maps = self._mapsCache
            self._notifyObservers()
            time.sleep(self.refreshInterval)
        print "Stopping thread main"
        self.memoryScanner.detach()
    
    # Warning: Observers should take care to make GUI calls from the main thread.
    # No guarantee is made the observer action will be called from the main thread.
    def addObserver(self, action):
        self._observers.append(action)
    
    def removeObserver(self, action):
        self._observers.remove(action)
    
    def _notifyObservers(self):
        for observer in self._observers:
            observer()
    
    def fetchStackTop(self): # Warning: This method can block briefly upon first call
        assert not self.stopped
        if not self._regsCache:
            time.sleep(0.25) # TODO: Fix
        if not self._regsCache:
            return 0
        return self._regsCache.rsp # TODO: Fix this for different OS bit-ness
    
    def fetchMemoryAt(self, start_addr, end_addr):
        assert not self.stopped
        memoryRange = MemoryRange((start_addr, end_addr), self)
        self._registerMemoryRange(memoryRange)
        self.triggerUpdate()
        return memoryRange
    
    def _registerMemoryRange(self, memory_range):
        self._visibleMemoryRanges.append(weakref.ref(memory_range, self._deregisterMemoryRange))
        return self._visibleMemoryRanges
    
    def _deregisterMemoryRange(self, memory_range_ref):
        print "Destroying memory range. What's left: "
        self._visibleMemoryRanges.remove(memory_range_ref)
        for r in self._visibleMemoryRanges:
            print r()._memoryRange