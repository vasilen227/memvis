#!/usr/bin/python

import wx
import wx.xrc
import wx.grid
import main_gui_frame
#import memory_grid_table

class Gui:
	def __init__(self, pid=None):
		self.app = wx.App()
		self.main_frame = main_gui_frame.FrameMain(None, pid, self.app)

	def mainloop(self):
		self.main_frame.Show(True)
		self.app.MainLoop()