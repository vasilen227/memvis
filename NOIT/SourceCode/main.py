#!/usr/bin/python
import gui
import backend
import sys #TODO: Remove this

# Use argparse

class Main:
    def __init__(self, pid):
        self.gui = gui.Gui(pid)
        self.pid = pid
    
    def start(self):
        self.gui.mainloop()

if __name__ == "__main__":
    # Call argparse
    if len(sys.argv) > 1:
        pid = int(sys.argv[1])
    else:
        pid = None
    main = Main(pid)
    main.start()