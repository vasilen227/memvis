#!/usr/bin/python

import wx
import wx.grid

class MainFrame(wx.Frame):

	def __init__(self, parent, title):
		super(MainFrame, self).__init__(parent, title=title,
			size=(400, 350))

		self.MainGui()
		self.Center()
		self.Show()

	def MainGui(self):

		mainPanel = wx.Panel(self)

		fontStyle = wx.SystemSettings_GetFont(wx.SYS_SYSTEM_FONT)
		fontStyle.SetPointSize(10)

		marginSizer = wx.BoxSizer(wx.VERTICAL)

		userInputStaticBox = wx.StaticBox(mainPanel, label="Process ID")
		userInputBoxSizer = wx.StaticBoxSizer(userInputStaticBox, wx.HORIZONTAL)
		
		userInputText = wx.TextCtrl(mainPanel)
		userInputText.SetFont(fontStyle)
		userInputBoxSizer.Add(userInputText, 1, flag=wx.ALL|wx.EXPAND, border=5)

		userInputButton = wx.Button(mainPanel, label="Proceed", size=(70, 30))
		userInputBoxSizer.Add(userInputButton, 0, flag=wx.ALL|wx.EXPAND, border=5)

		marginSizer.Add((-1, 10))

		gridStaticBox = wx.StaticBox(mainPanel, label="Memory Vizualisation")
		gridBoxSizer = wx.StaticBoxSizer(gridStaticBox, wx.HORIZONTAL)

		gridScroller = wx.ScrolledWindow(mainPanel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL)
		gridScroller.SetScrollRate(5, 8)
		gridBoxSizer.Add(gridScroller, 1, flag=wx.ALL|wx.EXPAND, border=5)

		memoryGrid = wx.grid.Grid(gridScroller, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		
		#Grid characteristics
		gridRows = 15
		memoryGrid.CreateGrid( gridRows, 16 )
		memoryGrid.EnableEditing( False )
		memoryGrid.EnableGridLines( True )
		memoryGrid.EnableDragGridSize( False )
		memoryGrid.SetMargins( 0, 0 )

		#Column characteristics
		memoryGrid.EnableDragColMove( False )
		memoryGrid.EnableDragColSize( True )
		memoryGrid.SetColLabelSize( 30 )
		memoryGrid.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		memoryGrid.SetColLabelValue(0, "00")
		memoryGrid.SetColLabelValue(1, "01")
		memoryGrid.SetColLabelValue(2, "02")
		memoryGrid.SetColLabelValue(3, "03")
		memoryGrid.SetColLabelValue(4, "04")
		memoryGrid.SetColLabelValue(5, "05")
		memoryGrid.SetColLabelValue(6, "06")
		memoryGrid.SetColLabelValue(7, "07")
		memoryGrid.SetColLabelValue(8, "08")
		memoryGrid.SetColLabelValue(9, "09")
		memoryGrid.SetColLabelValue(10, "0A")
		memoryGrid.SetColLabelValue(11, "0B")
		memoryGrid.SetColLabelValue(12, "0C")
		memoryGrid.SetColLabelValue(13, "0D")
		memoryGrid.SetColLabelValue(14, "0E")
		memoryGrid.SetColLabelValue(15, "0F")

		#Row characteristics
		gridCellHeight = 40
		memoryGrid.EnableDragRowSize( True )
		memoryGrid.SetRowLabelSize( 130 )
		memoryGrid.SetDefaultRowSize(gridCellHeight, resizeExistingRows=True)
		memoryGrid.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		#Cell characteristics
		memoryGrid.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )

		#gridScroller.SetSizer( SizerScrollStack )
		#gridScroller.Layout()
		#SizerScrollStack.Fit( gridScroller )

		buttonsBoxSizer = wx.BoxSizer(wx.VERTICAL)
		gridBoxSizer.Add(buttonsBoxSizer, 0, flag=wx.ALL|wx.EXPAND, border=5)

		scrollUpButton = wx.Button(mainPanel, label="Up", size=(70, 30))
		#gridBoxSizer.Add(scrollUpButton, 0, flag=wx.ALL|wx.EXPAND, border=5)
		buttonsBoxSizer.Add(scrollUpButton, 0, flag=wx.ALL|wx.EXPAND, border=5)

		scrollDownButton = wx.Button(mainPanel, label="Down", size=(70, 30))
		#gridBoxSizer.Add(scrollDownButton, 0, flag=wx.ALL|wx.EXPAND, border=5)
		buttonsBoxSizer.Add(scrollDownButton, 0, flag=wx.ALL|wx.EXPAND, border=5)

if __name__ == '__main__':
  
    app = wx.App()
    MainFrame(None, title='Memory Vizualisation')
    app.MainLoop()
