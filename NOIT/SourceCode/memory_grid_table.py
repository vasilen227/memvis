#!/usr/bin/python

from wx import *
from wx.grid import *
import backend
import memory_scanner as ms

app = None

EVT_BACKEND_UPDATE = wx.NewEventType()

class MemoryGridTable(PyGridTableBase):
    def __init__(self, backend, app, grid):
        PyGridTableBase.__init__(self)
        self.backend = backend
        backend.addObserver(self.updateNotification)
        start_addr = backend.fetchStackTop()
        end_addr = start_addr + 16*10 # ?!?!?!?
        print start_addr, end_addr
        self.memoryRange = backend.fetchMemoryAt(start_addr, end_addr)
        self.app = app
        self.grid = grid
        self.EVT_BACKEND_UPDATE_BINDER = wx.PyEventBinder(EVT_BACKEND_UPDATE, 1)
        self.app.Bind(self.EVT_BACKEND_UPDATE_BINDER, self.refreshData)
        
    def updateNotification(self, *args):
        app = self.app
        evt = wx.PyCommandEvent(EVT_BACKEND_UPDATE, -1)
        wx.PostEvent(app, evt)
    
    def refreshData(self, *args):
        self.grid.ForceRefresh()
    
    def moveDown(self, n=1):
        cur_start = self.memoryRange.start
        cur_end = self.memoryRange.end
        self.memoryRange = self.backend.fetchMemoryAt(cur_start - 16, cur_end - 16)
        self.refreshData()
    
    def moveUp(self, n=1):
        cur_start = self.memoryRange.start
        cur_end = self.memoryRange.end
        self.memoryRange = self.backend.fetchMemoryAt(cur_start + 16, cur_end + 16)
        self.refreshData()
    
    def GetNumberRows(self):
        return 10 # FIX THIS
	
    def GetNumberCols(self):
        return 16

    def IsEmptyCell(self, row, col):
        #try:
        #    return not self.data[row][col]
        #except IndexError:
        #    return True
        return False
    
    def GetValue(self, row, col):
        val = self.memoryRange.data[(row*16+col)]
        if val is None:
            return 'Loading...'
        #return hex(ord(val))
        try:
            unichr(ord(val))
            return ('{} \n {}'.format(hex(ord(val)), unichr(ord(val))))
        except UnicodeEncodeError:
            return 'NR'
        #return ('{} --> {}'.format(hex(ord(val)), unichr(ord(val))))

    def SetValue(self, row, col, value):
        raise NotImplementedError()
        #try:
        #    self.data[row][col] = value
        #except IndexError:
        #    self.data.append([''] * self.GetNumberCols())
        #    self.SetValue(row, col, value)
        #    msg = wxGridTableMessage(self,                             # The table
        #                             wxGRIDTABLE_NOTIFY_ROWS_APPENDED, # what we did to it
        #                             1)                                # how many
        #    self.GetView().ProcessTableMessage(msg)
    
    def GetColLabelValue(self, col):
        return hex(col)
    
    def GetRowLabelValue(self, row):
        val = self.backend._registerMemoryRange(self.memoryRange)
        return 'Row%d' % (row,)

"""
class CustTableGrid(Grid):
    def __init__(self, parent, data, app):
        Grid.__init__(self, parent, -1)
        self.table = MemoryGridTable(gbackend, app, self)
        self.SetTable(self.table, True)
        self.SetRowLabelSize(0)
        self.SetMargins(0,0)
        self.AutoSizeColumns(False)
        EVT_GRID_CELL_LEFT_DCLICK(self, self.OnLeftDClick)
    def OnLeftDClick(self, evt):
        if self.CanEnableCellControl():
            self.EnableCellEditControl()
#---------------------------------------------------------------------------
class TestFrame(Frame):
    def __init__(self, parent, app):
        Frame.__init__(self, parent, -1, "Custom Table, data driven Grid  Demo", size=(640,480))
        self.data = [
            [1010, "The foo doesn't bar", "major", 1, 'MSW', 1, 1, 1, 1.12],
            [1011, "I've got a wicket in my wocket", "wish list", 2, 'other', 0, 0, 0, 1.50],
            [1012, "Rectangle() returns a triangle", "critical", 5, 'all', 0, 0, 0, 1.56]
            ]
        self.app = app
        self.initwin()
    def initwin(self):
        self.ref= None
        self.p = Panel(self, -1, style=0)
        b = Button(self.p, -1, "Add line...")
        b.SetDefault()
        EVT_BUTTON(self, b.GetId(), self.OnButton)
        self.bs = BoxSizer(VERTICAL)
        self.grid = CustTableGrid(self.p,self.data,self.app)
        self.bs.Add(self.grid, 1, GROW|ALL, 5)
        self.bs.Add(b)
        self.p.SetSizer(self.bs)
    def OnButton(self, evt):
        self.data.append([1010, "Added line", "major", 1, 'MSW', 1, 1, 1, 1.12])
        sz= self.p.GetSize()
        self.p.Destroy()
        self.initwin()
        self.p.SetSize(sz)
        print "button selected"
#-------------------------------------------------------------------------------
class mApp(App):
    def OnInit(self):
        frame= TestFrame(None, self)
        frame.Show(True)
        return True
#-------------------------------------------------------------------------------
gbackend = backend.Backend(1)
gbackend.start()
app = mApp(0)
app.MainLoop()
#-------------------------------------------------------------------------------
print "Stopping..."
gbackend.stop()
"""